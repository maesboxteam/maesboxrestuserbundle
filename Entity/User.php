<?php

namespace Maesbox\RestUserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

use Maesbox\RestUserBundle\Model\User\UserInterface;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(fields="email", message="Email already taken")
 * @UniqueEntity(fields="username", message="Username already taken")
 * @UniqueEntity(fields="token", message="token already exist")
 * @Serializer\ExclusionPolicy("all")
 */
abstract class User implements UserInterface
{
	 /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Email()
	 * @Serializer\Expose
     * @Serializer\Groups({"User"})
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Assert\NotBlank()
	 * @Serializer\Expose
     * @Serializer\Groups({"User"})
     */
    protected $username;

    /**
     * @Assert\NotBlank()
     * @Assert\Length(max=4096)
     */
    protected $plain_password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $password;

    /**
     * @ORM\Column(name="is_enabled", type="boolean")
     */
    protected $enabled = false;
	
	/**
	 * @ORM\Column(name="salt")
	 */
    protected $salt;
	
	/**
	 * @ORM\Column(name="roles", type="json_array")
	 */
    protected $roles;
	
	/**
	 * @ORM\Column(name="token", unique=true, nullable=true )
	 */
	protected $token;
	
	/**
	 * @ORM\Column(name="created_at", type="datetime", nullable=true)
	 * @var DateTime 
	 */
	protected $created_at;
	
	/**
	 * @ORM\Column(name="updated_at", type="datetime", nullable=true)
	 * @var DateTime 
	 */
	protected $updated_at;
	
	/**
	 * @ORM\Column(name="enabled_at", type="datetime", nullable=true)
	 * @var DateTime 
	 */
	protected $enabled_at;
	
	public function __toString() 
    { 
        return $this->getUsername(); 
    }
     
 
    public function isEqualTo(UserInterface $user)
    {
        return $this->username === $user->getUsername();
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getPlainPassword()
    {
        return $this->plain_password;
    }

    public function setPlainPassword($password)
    {
        $this->plain_password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

	public function getRoles()
	{
		return $this->roles;
	}
	
    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return User
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get isEnabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }
	
	public function getsalt()
	{
		return $this->salt;
	}

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set token
     *
     * @param string $token
     *
     * @return User
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * Get validationToken
     *
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }
	
	/**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->created_at = $createdAt;

        return $this;
    }
	
	/**
	 * @ORM\PrePersist
	 * @return $this
	 */
	public function setCreatedAtValue()
    {
        $this->created_at = new \DateTime();
    }
	
    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updated_at = $updatedAt;

        return $this;
    }
	
	/**
	 * @ORM\PreUpdate
	 */
	public function setUpdateAtValue()
	{
		$this->updated_at = new \DateTime();
	}

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * Set enabledAt
     *
     * @param \DateTime $enabledAt
     *
     * @return User
     */
    public function setEnabledAt($enabledAt)
    {
        $this->enabled_at = $enabledAt;

        return $this;
    }

    /**
     * Get enabledAt
     *
     * @return \DateTime
     */
    public function getEnabledAt()
    {
        return $this->enabled_at;
    }
	
	public function eraseCredentials()
    {
    }

	public function isAccountNonExpired()
	{
		return true;
	}

	public function isAccountNonLocked()
	{
		return true;
	}

	public function isCredentialsNonExpired()
	{
		return true;
	}

	public function isEnabled()
	{
		return $this->enabled;
	}
}
