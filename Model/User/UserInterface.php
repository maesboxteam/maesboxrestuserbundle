<?php

namespace Maesbox\RestUserBundle\Model\User;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

interface UserInterface extends AdvancedUserInterface
{
	public function getEmail();
	
	public function getToken();
	
	public function setToken($token);
	
	public function setEnabled($is_enabled);
	
	public function setEnabledAt($is_enabled);
}