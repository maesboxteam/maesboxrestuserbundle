<?php

namespace Maesbox\RestUserBundle\Model\Event;

use Maesbox\RestUserBundle\Model\Event\UserEvent;

class UserRegistrationEvent extends UserEvent
{
	const USER_REGISTRATION_SUBMITED = "user_registration_submitted";

	const USER_REGISTRATION_EMAIL = "user_registration_email";
	
	const USER_REGISTRATION_VALIDATION = "user_registration_validation";
	
	const USER_REGISTRATION_COMPLETED = "user_registration_completed";
	
}
