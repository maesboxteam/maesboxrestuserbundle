<?php
namespace Maesbox\RestUserBundle\Model\Event;

use Symfony\Component\EventDispatcher\Event;

use Maesbox\RestUserBundle\Model\User\UserInterface;

abstract class UserEvent extends Event
{
	
	/**
	 * @var UserInterface 
	 */
	protected $user;
	
	/**
	 * @param UserInterface $user
	 */
	public function __construct(UserInterface $user = null)
	{
		$this->setUser($user);
	}


	/**
	 * @param UserInterface $user
	 * @return $this
	 */
	public function setUser(UserInterface $user = null)
	{
		$this->user = $user;
		return $this;
	}
	
	/**
	 * @return UserInterface
	 */
	public function getUser()
	{
		return $this->user;
	}
}