<?php

namespace Maesbox\RestUserBundle\Model\Event\Subscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Exception\InvalidParameterException;

use Doctrine\Common\Persistence\ObjectManager;

use Swift_Mailer;
use Swift_Message;

use Psr\Log\LoggerInterface;

use Maesbox\RestUserBundle\Model\Event\UserRegistrationEvent;
use Maesbox\RestUserBundle\Model\User\UserInterface;

class RegistrationSubscriber implements EventSubscriberInterface
{
	/**
	 * @var ObjectManager 
	 */
	protected $object_manager;
	
	/**
	 * @var LoggerInterface 
	 */
	protected $logger;
	
	/**
	 * @var EventDispatcherInterface 
	 */
	protected $event_dispatcher;
	
	/**
	 * @var Swift_Mailer 
	 */
	protected $mailer;
	
	/**
	 * @var boolean 
	 */
	protected $email_validation;
	
	public function __construct(ObjectManager $manager, EventDispatcherInterface $dispatcher, Swift_Mailer $mailer, LoggerInterface $logger, $email_validation)
	{
		$this->setObjectManager($manager)
			->setEventDispatcher($dispatcher)
			->setMailer($mailer)
			->setLogger($logger)
			->setEmailValidation($email_validation);
	}
	
	/**
	 * @return array
	 */
	public static function getSubscribedEvents()
	{
		return [
			UserRegistrationEvent::USER_REGISTRATION_SUBMITED => [
				[ "processSubmittedRegistration", 0 ]
			],		
			UserRegistrationEvent::USER_REGISTRATION_EMAIL => [
				[ "processRegistrationEmail", 0 ] 
			],
			UserRegistrationEvent::USER_REGISTRATION_VALIDATION => [
				[ "processRegistrationValidation", 0 ]
			],
			UserRegistrationEvent::USER_REGISTRATION_COMPLETED => [
				[ "processRegistrationCompleted", 0]
			]
		];
	}
	
	/**
	 * @param UserRegistrationEvent $event
	 * @throws InvalidParameterException
	 */
	public function processSubmittedRegistration(UserRegistrationEvent $event)
	{
		$this->getLogger()->info("user registration submitted", [
			"class" => __CLASS__,
			"method" => __FUNCTION__,
			"user" => $event->getUser()
		]);
		
		$this->saveUser($event->getUser());
		
		if($this->getEmailValidation()){
			if(parse_url($this->getEmailValidation()) !== FALSE) {
				$this->getEventDispatcher()->dispatch(UserRegistrationEvent::USER_REGISTRATION_EMAIL, $event);
			} else {
				throw new InvalidParameterException("invalid url in email_validation parameter");
			}
			
		} else {
			$this->getEventDispatcher()->dispatch(UserRegistrationEvent::USER_REGISTRATION_VALIDATION, $event);
		}
	}
	
	/**
	 * @param UserRegistrationEvent $event
	 */
	public function processRegistrationEmail(UserRegistrationEvent $event)
	{
		// on genere le token 
		$token = base64_encode(
			json_encode([
				"key" => base64_encode(random_bytes(20).$event->getUser()->getUsername()),
				"expiration" => new \DateTime("+1 Week")
			]));
		
		$event->getUser()->setToken($token);

		// on save le user
		$this->saveUser($event->getUser());
		
		$this->sendEmail($event->getUser());
		
		$this->getLogger()->info("user registration email sent", [
			"class" => __CLASS__,
			"method" => __FUNCTION__,
			"user" => $event->getUser()
		]);
	}
	
	/**
	 * @param UserRegistrationEvent $event
	 */
	public function processRegistrationValidation(UserRegistrationEvent $event)
	{
		$event->getUser()->setToken(null);
		$event->getUser()->setEnabled(true);
		$event->getUser()->setEnabledAt(new \DateTime());
		
		$this->saveUser($event->getUser());
		
		$this->getLogger()->info("user registration validated", [
			"class" => __CLASS__,
			"method" => __FUNCTION__,
			"user" => $event->getUser()
		]);
		
		$this->getEventDispatcher()->dispatch(UserRegistrationEvent::USER_REGISTRATION_COMPLETED, $event);
	}
	
	public function processRegistrationCompleted(UserRegistrationEvent $event)
	{
		
		$this->getLogger()->info("user registration completed", [
			"class" => __CLASS__,
			"method" => __FUNCTION__,
			"user" => $event->getUser()
		]);
	}
	
	protected function saveUser(UserInterface $user)
	{
		$this->getLogger()->info("user data saved", [
			"class" => __CLASS__,
			"method" => __FUNCTION__,
			"user" => $user
		]);
		
		$this->getObjectManager()->persist($user);
		$this->getObjectManager()->flush();
	}
	
	protected function sendEmail(UserInterface $user)
	{
		$message = Swift_Message::newInstance();
		
		$message->setSubject("confirm your email");
		$message->addTo($user->getEmail(), $user->getUsername());
		$message->addFrom("no-reply@og-inspector.fr.nf");
		$message->setBody($this->processValidationUrl($user->getToken()));
		
		// on envoie l'email
		$this->getMailer()->send($message);
	}
	
	protected function processValidationUrl($token)
	{
		if(strpos($this->getEmailValidation(), "{token}") !== FALSE){
			$url = str_replace("{token}", $token, $this->getEmailValidation());
		} else {
			
		}
		
		return $url;
	}
	
	/**
	 * @return EventDispatcherInterface
	 */
	protected function getEventDispatcher()
	{
		return $this->event_dispatcher;
	}
	
	/**
	 * @param EventDispatcherInterface $dispatcher
	 * @return $this
	 */
	protected function setEventDispatcher(EventDispatcherInterface $dispatcher)
	{
		$this->event_dispatcher = $dispatcher;
		return $this;
	}
	
	/**
	 * @param Swift_Mailer $mailer
	 * @return $this
	 */
	protected function setMailer(Swift_Mailer $mailer)
	{
		$this->mailer = $mailer;
		return $this;
	}
	
	/**
	 * @return Swift_Mailer
	 */
	protected function getMailer()
	{
		return $this->mailer;
	}
	
	/**
	 * @return LoggerInterface
	 */
	protected function getLogger()
	{
		return $this->logger;
	}

	/**
	 * @param LoggerInterface $logger
	 * @return $this
	 */
	protected function setLogger(LoggerInterface $logger)
	{
		$this->logger = $logger;
		return $this;
	}
	
	/**
	 * @return boolean|string
	 */
	protected function getEmailValidation()
	{
		return $this->email_validation;
	}
	
	/**
	 * @param boolean|string $email_validation
	 * @return $this
	 */
	protected function setEmailValidation($email_validation)
	{
		$this->email_validation = $email_validation;
		return $this;
	}
	
	/**
	 * @param ObjectManager $manager
	 * @return $this
	 */
	protected function setObjectManager(ObjectManager $manager)
	{
		$this->object_manager = $manager;
		return $this;
	}
	
	/**
	 * @return ObjectManager
	 */
	protected function getObjectManager()
	{
		return $this->object_manager;
	}
}

