<?php

namespace Maesbox\RestUserBundle\Model\Event\Listener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\JWTCreatedEvent;
use Symfony\Component\HttpFoundation\RequestStack;

class UserPayloadListener
{
	/**
	 * @param JWTCreatedEvent $event
	 *
	 * @return void
	 */
	public function onJWTCreated(JWTCreatedEvent $event)
	{
		$payload = $event->getData();
		
		$reflection = new \ReflectionObject($event->getUser());
		if($reflection->hasMethod("getId")) {
			$payload["id"] = $event->getUser()->getId();
		}

		$event->setData($payload);
	}
}

