<?php

namespace Maesbox\RestUserBundle\Model\Form\Type;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class ChangePasswordType extends AbstractType
{
	protected $userclass;
	
	public function __construct($userclass)
	{
		$this->setUserClass($userclass);
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
			->add('old_password', PasswordType::class, array(
                'mapped' => false,
				'label' => "old password"
            ))
            ->add('plain_password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Password'),
                'second_options' => array('label' => 'Repeat Password'),
            ))
			;
    }
	
	/**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class'            => $this->getUserClass(),
			'allow_extra_fields'    => true,
            'csrf_protection'       => false,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return '';
    }
	
	protected function setUserClass($userclass)
	{
		$this->userclass = $userclass;
		return $this;
	}
	
	protected function getUserClass()
	{
		return $this->userclass;
	}
}

