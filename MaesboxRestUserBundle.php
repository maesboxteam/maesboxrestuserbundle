<?php

namespace Maesbox\RestUserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

use Maesbox\RestUserBundle\DependencyInjection\Compiler\RestUserCompilerPass;
 
class MaesboxRestUserBundle extends Bundle
{
	/**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new RestUserCompilerPass());

        parent::build($container);
    }
}
