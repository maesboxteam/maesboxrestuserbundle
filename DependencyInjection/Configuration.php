<?php

namespace Maesbox\RestUserBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder    = new TreeBuilder();
        $rootNode       = $treeBuilder->root('maesbox_rest_user');
		
		$rootNode->children()
				->scalarNode("user_class")->cannotBeEmpty()->isRequired()->end()
				->scalarNode("email_validation")->cannotBeEmpty()->defaultFalse()->end()
			->end();
		
        return $treeBuilder;
    }
}