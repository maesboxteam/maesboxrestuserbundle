<?php

namespace Maesbox\RestUserBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;


class RestUserCompilerPass implements CompilerPassInterface
{	
    public function process(ContainerBuilder $container)
    {
		$container->getParameterBag()->set("maesbox.rest_user.user_class", $this->getUserClass($container));
		$container->getParameterBag()->set("maesbox.rest_user.email_validation", $this->getEmailValidation($container));
	}
	
	protected function getConfig(ContainerBuilder $container)
	{		
		$config = $container->getExtensionConfig("maesbox_rest_user");
		return $config[0];
	}
	
	protected function getUserClass(ContainerBuilder $container)
	{
		$config = $this->getConfig($container);
		return $config["user_class"];
	}
	
	protected function getEmailValidation(ContainerBuilder $container)
	{
		$config = $this->getConfig($container);
		return (isset($config["email_validation"]))?$config["email_validation"]:false;
	}
}
