<?php

namespace Maesbox\RestUserBundle\Controller;

use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;

use Maesbox\RestUserBundle\Model\Form\Type\RegisterType;

use Maesbox\RestUserBundle\Model\Form\Type\ResetType;

use Maesbox\RestUserBundle\Model\Event\UserRegistrationEvent;

/**
 * @Rest\RouteResource("Validate", pluralize=false)
 */
class ValidateController extends FOSRestController
{
	/**
     * @ApiDoc(
     *	section="Rest Security",
     *  description="register a user",
     *  views={
     *			"default",
     *			"admin",
     *			"user"
     *  },
     *  tags={
     *         "dev"
     *     }
     * )
     */
	public function getAction(Request $request, $token)
	{
		$view = $this->view();
		
		$repository = $this->container->get('doctrine')->getManager()->getRepository($this->container->getParameter("maesbox.rest_user.user_class"));
		
		$user = $repository->findOneBy([
			"token" => $token,
			"enabled" => false
		]);
		
		if($user) {
			$event = new UserRegistrationEvent($user);

			$this->get('event_dispatcher')->dispatch(UserRegistrationEvent::USER_REGISTRATION_VALIDATION, $event);

			$authenticationSuccessHandler = $this->container->get('lexik_jwt_authentication.handler.authentication_success');
			$view->setData(json_decode($authenticationSuccessHandler->handleAuthenticationSuccess($user)->getContent(), true));
		} else {
			$view->setStatusCode(404);
		}
		
		return $this->handleView($view);
	}
}
