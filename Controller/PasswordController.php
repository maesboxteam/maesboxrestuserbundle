<?php

namespace Maesbox\RestUserBundle\Controller;

use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;


use Maesbox\RestUserBundle\Model\Form\Type\ResetType;
use Maesbox\RestUserBundle\Model\Form\Type\ForgottenType;
use Maesbox\RestUserBundle\Model\Form\Type\ChangePasswordType;

use Maesbox\RestUserBundle\Model\Event\UserRegistrationEvent;

/**
 * @Rest\RouteResource("Password", pluralize=false)
 */
class PasswordController extends FOSRestController
{
	/**
	 * @Secure(roles="ROLE_USER")
	 * @ApiDoc(
     *	section="Rest Security",
     *  description="change a user password",
	 *  input = "Maesbox\RestUserBundle\Model\Form\Type\ChangePasswordType",
     *  views={
     *			"default",
     *			"admin",
     *			"user"
     *  },
     *  tags={
     *         "dev"
     *     }
     * )
	 */
	public function postChangeAction(Request $request)
	{
		$view = $this->view();
		
		$repository = $this->container->get('doctrine')->getManager()->getRepository($this->container->getParameter("maesbox.rest_user.user_class"));
		
		$form = $this->createForm(ChangePasswordType::class, $this->getUser());
		
		$form->handleRequest($request);
		
		if($form->isValid()) {
			
		} else {
			$view->setData($form);
			$view->setStatusCode(400);
		}
		
		return $this->handleView($view);
	}
	
	/**
	 * @ApiDoc(
     *	section="Rest Security",
     *  description="reset a user password",
	 *  input = "Maesbox\RestUserBundle\Model\Form\Type\ForgottenType",
     *  views={
     *			"default",
     *			"admin",
     *			"user"
     *  },
     *  tags={
     *         "dev"
     *     }
     * )
	 */
	public function postForgottenAction(Request $request)
	{
		$view = $this->view();
		$repository = $this->container->get('doctrine')->getManager()->getRepository($this->container->getParameter("maesbox.rest_user.user_class"));
		
		$form = $this->createForm(ForgottenType::class);
		
		$form->handleRequest($request);
		
		if($form->isValid()) {
			$data = $form->getData();
			$user = $repository->findOneBy(["email" => $data['email']]);
			
			
			if($user){
				$view->setData($user);
			} else {
				throw new NotFoundHttpException("email not found");
			}
		} else {
			$view->setData($form);
			$view->setStatusCode(400);
		}
		
		return $this->handleView($view);
	}
	
	/**
	 * @ApiDoc(
     *	section="Rest Security",
     *  description="reset a user password",
	 *  input = "Maesbox\RestUserBundle\Model\Form\Type\ResetType",
     *  views={
     *			"default",
     *			"admin",
     *			"user"
     *  },
     *  tags={
	 *		   "upgradable"="#FFCC11",
     *         "dev"
     *     }
     * )
	 */
	public function postResetAction(Request $request, $token)
	{
		$view = $this->view();
		
		$manager = $this->container->get('doctrine')->getManager();
		
		$repository = $manager->getRepository($this->container->getParameter("maesbox.rest_user.user_class"));
		
		$user = $repository->findOneBy([
			"token" => $token,
			"enabled" => true
		]);
		
		if($user) {
			$form = $this->createForm(ResetType::class, $user);
		
			$form->handleRequest($request);

			if($form->isValid()) {

				//encoding password
				$password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
				$user->setPassword($password);

				$manager->persist($user);
				$manager->flush();
			} else {
				$view->setData($form);
				$view->setStatusCode(400);
			}
		} else {
			$view->setStatusCode(404);
		}
		
		
		return $this->handleView($view);
	}
}
