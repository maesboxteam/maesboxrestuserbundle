<?php

namespace Maesbox\RestUserBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


use FOS\RestBundle\Controller\FOSRestController;

use Maesbox\RestUserBundle\Model\Form\Type\RegisterType;

use Maesbox\RestUserBundle\Model\Event\UserRegistrationEvent;

/**
 * @Rest\RouteResource("Register", pluralize=false)
 */
class RegisterController extends FOSRestController
{
	/**
     * @ApiDoc(
     *	section="Rest Security",
     *  description="register a user",
	 *  input = "Maesbox\RestUserBundle\Model\Form\Type\RegisterType",
     *  views={
     *			"default",
     *			"admin",
     *			"user"
     *  },
     *  tags={
     *         "stable"="#00AA00",
     *     }
     * )
     */
	public function postAction(Request $request)
	{
		$view = $this->view();
		
		$form = $this->createForm(RegisterType::class);
		
		$form->handleRequest($request);
		
		if($form->isValid()){
			$user = $form->getData();
			//creating salt
			$salt = base64_encode(random_bytes(20).$user->getUsername());
			$user->setSalt($salt);
			
			//encoding password
			$password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
			
			// set user roles
			$user->setRoles(array("ROLE_USER"));
			
			$event = new UserRegistrationEvent($user);
			
			$this->get('event_dispatcher')->dispatch(UserRegistrationEvent::USER_REGISTRATION_SUBMITED, $event);
			
			
			// check email validation
			if($this->getParameter("maesbox.rest_user.email_validation") !== false) {
				$view->setData(["message" => "account created. a validation email has been sent."]);
			} else {
				//$view->setData(["message" => "account created"]);
				$authenticationSuccessHandler = $this->container->get('lexik_jwt_authentication.handler.authentication_success');
				$view->setData(json_decode($authenticationSuccessHandler->handleAuthenticationSuccess($user)->getContent(), true));
			}
		} else {
			$view->setStatusCode(400);
			$view->setData($form);
		}
		
		return $this->handleView($view);
	}
}
