<?php

namespace Maesbox\RestUserBundle\Controller;

use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use JMS\SecurityExtraBundle\Annotation\Secure;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use FOS\RestBundle\Controller\FOSRestController;

use Maesbox\RestUserBundle\Model\Form\Type\RegisterType;

use Maesbox\RestUserBundle\Model\Form\Type\ResetType;

use Maesbox\RestUserBundle\Model\Event\UserRegistrationEvent;

/**
 * @Rest\RouteResource("Authentication", pluralize=false)
 */
class AuthenticationController extends FOSRestController
{
	/**
     * @ApiDoc(
     *	section="Rest Security",
     *  description="register a user",
	 *  input = "Maesbox\RestUserBundle\Model\Form\Type\RegisterType",
     *  views={
     *			"default",
     *			"admin",
     *			"user"
     *  },
     *  tags={
     *         "dev"
     *     }
     * )
     */
	public function postAction(Request $request, $provider)
	{
		$view = $this->view();
		
		$view->setData($provider);
		
		/*$form = $this->createForm(RegisterType::class);
		
		$form->handleRequest($request);
		
		if($form->isValid()){
			$user = $form->getData();
			//creating salt
			$salt = base64_encode(random_bytes(20).$user->getUsername());
			$user->setSalt($salt);
			
			//encoding password
			$password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
			
			// set user roles
			$user->setRoles(array("ROLE_USER"));
			
			$event = new UserRegistrationEvent($user);
			// check email validation
			$this->get('event_dispatcher')->dispatch(UserRegistrationEvent::USER_REGISTRATION_SUBMITED, $event);
			
			
			
			if($this->getParameter("maesbox.rest_user.email_validation") !== false) {
				$view->setData(["message" => "account created. a validation email has been sent."]);
			} else {
				//$view->setData(["message" => "account created"]);
				$authenticationSuccessHandler = $this->container->get('lexik_jwt_authentication.handler.authentication_success');
				$view->setData($authenticationSuccessHandler->handleAuthenticationSuccess($user));
			}
		} else {
			$view->setStatusCode(400);
			$view->setData($form);
		}*/
		
		return $this->handleView($view);
	}
}
